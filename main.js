Array.prototype.writeIndices = function(n) {
    for( var i = 0; i < (n || this.length); ++i ) this[i] = i;
    return this;
};

Array.prototype.writeValues = function(n, val) {
    for( var i = 0; i < (n || this.length); ++i ) this[i] = val;
    return this;
};

sigma.classes.graph.addMethod('neighbors', function(nodeId) {
  var k;
  var self = this;
  var neighbors = {};
  var children = USERS[parseInt(nodeId)].children || [];

  children.forEach(function(k) {
    neighbors[k] = USERS[k];
  });

  return neighbors;
});

SIGMA = new sigma('graph-container');
USERS = [];
SELECTED_VERSION = null;
VERSION_COLORS = {
  "1" : '#e00',
  "2" : '#00f',
  "3" : '#0f0',
  "4" : '#f90'
};

//////////////////////////////////////////////////
// Functions Related to Manipulating Graph Data //
//////////////////////////////////////////////////
function total_infection(nodeId, version) {
  print("Infecting node " + nodeId + " with version " + SELECTED_VERSION);
  var totalInfectionCount = _total_infection(nodeId, version);
  print("..." + totalInfectionCount + " nodes infected total");
}

function _total_infection(nodeId, version, old_infect_count, use_temp_version) {
  if (typeof(old_infect_count) == "undefined") old_infect_count = 0;

  // if the note we're trying to infect already had the version we want
  // no need to keep propogating, avoids circular loop
  if (use_temp_version) {
    if (USERS[nodeId].version_temp == version) {
      return old_infect_count;
    }
  }
  else {
    if (USERS[nodeId].version == version) {
      return old_infect_count;
    }
  }

  // add 1 for infecting the given node
  var count = old_infect_count + 1;
  if (use_temp_version) {
    setTempNodeVersion(nodeId, version);
  }
  else {
    setNodeVersion(nodeId, version);
  }

  // recursive call for children
  for(var i=0; i < USERS[nodeId].children.length; i++) {
    var childId = USERS[nodeId].children[i];
    count += _total_infection(childId, version, 0, use_temp_version);
  }

  // recursive call for parent
  for(var i=0; i < USERS[nodeId].parents.length; i++) {
    var parentId = USERS[nodeId].parents[i];
    count += _total_infection(parentId, version, 0, use_temp_version);
  }

  return count;
}

function limited_infection(count, version) {
  print("Trying to infecting " + count + " nodes with version " + SELECTED_VERSION);
  var roots = _get_root_nodes();

  var singleRoots = [];
  var groupRoots = {};

  for(id in roots) {
    if (roots[id] == 1) {
      singleRoots.push(id);
    }
    else {
      groupRoots[id] = roots[id];
    }
  }

  // assuming we are usually releasing features to a small subset of all users
  // e.g., thousands, not tens/hundreds of thousands

  var slack = 0;
  do {
    var min = (count - singleRoots.length - slack) < 0 ? 0 : (count - singleRoots.length - slack);
    var max = (count + slack) > USERS.length ? USERS.length : count + slack;
    var result = _solve_for_sum_group(groupRoots, min-slack, count+slack);

    if (result) {
      print("Found limited infection result: " + result.groups + ", with " + result.singles + " single nodes");

      var infectionCount = 0;
      // apply total_infection to each of the group
      result.groups.forEach(function(group) {
        infectionCount += _total_infection(group, SELECTED_VERSION);
      });

      // total_infection the singles
      // we may not have enough singles to fill what we need (if we needed slack in the
      // subet sum solution).  then we'll just stop and end up with a slightly smaller
      // set of infection.
      for(var i=0; i < result.singles && i < singleRoots.length; i++) {
        infectionCount += _total_infection(singleRoots[i], SELECTED_VERSION);
      }

      print("Infected total of " + infectionCount + " nodes with version " + SELECTED_VERSION);
      return;
    }
    else {
      print("No groups found for limited infection of " + count + " nodes");
      slack += Math.round(USERS.length * 0.02);
      print("Trying again with slack of " + slack + "");
    }
  }
  while(result == null && count + slack < USERS.length && count - slack > 0);

  print("Given up!");
}

// following solution suggested for subset sum on wikipedia
function _solve_for_sum_group(groups, min, max)
{
  if (min < 0) min = 0;

  var subsets = [];
  subsets[0] = [];

  for (var group in groups)
  {
    for (var s = min-1; s >= 0; --s)
    {
      if (s in subsets)
      {
        var sum = s + groups[group];

        if (!(sum in subsets))
        {
          subsets[sum] = subsets[s].concat(group);

          if (sum >= min && sum <= max)
          {
            return {
              groups : subsets[sum],
              singles : max - sum
            };
          }
        }
      }
    }
  }

  return null;
}

// returns a hash
function _get_root_nodes() {
  var roots = {};
  var tempVersion = chance.natural();

  for(var i=0; i < USERS.length; i++) {
    if (USERS[i].parents.length == 0) {
      var count = _total_infection(USERS[i].id, tempVersion, 0, true);

      if (count > 0) {
        roots[i] = count;
      }
    }
  }

  return roots;
}

function setTempNodeVersion(nodeId, version) {
  USERS[nodeId]["version_temp"] = version;
}

function setNodeVersion(nodeId, version) {
  // update our own data
  USERS[nodeId]["version"] = version;
  USERS[nodeId]["color"] = VERSION_COLORS[version];
  USERS[nodeId]["originalColor"] = VERSION_COLORS[version];
}

/////////////////////////
// DOM event listeners //
/////////////////////////
$("#initRandomNodesBtn").click( function() {
  initRandomNodes();
});

$("#removeOrphanedNodesBtn").click( function() {
  removeOrphanedNodes();
});

$("#triggerLimitedInfectionBtn").click( function() {
  if (!SELECTED_VERSION) {
    print("Please select a version to do limit infection first.");
    return;
  }

  var count = parseInt($("#limitedInfectionCount").val());
  limited_infection(count, SELECTED_VERSION);
  updateGraphNodes();
});

$("#resetAllNodeToVersion1Btn").click( function() {
  for(var i=0; i < USERS.length; i++) {
    setNodeVersion(USERS[i].id, 1);
  }
  updateGraphNodes();
  print("All nodes reset to Version 1");
});

$("input[type='radio']").change( function() {
  SELECTED_VERSION = $(this).val();
  print("You've selected version " + SELECTED_VERSION);
});


// When a node is clicked with a given version, triggers the infection code
SIGMA.bind('clickNode', function(e) {
  var nodeId = e.data.node.id;

  if (SELECTED_VERSION) {
    total_infection(nodeId, SELECTED_VERSION);
    updateGraphNodes();
  }
});

  // When a node is hovered over, we check for each node to see
  // if it is a neighbor of the clicked one. If not, we set its color as
  // grey, and else, it takes its original color.
SIGMA.bind('overNode', function(e) {

  var nodeId = e.data.node.id;

  print("Showing neighbors for " + nodeId);
  var toKeep = SIGMA.graph.neighbors(nodeId);

  toKeep[nodeId] = e.data.node;

  SIGMA.graph.nodes().forEach(function(n) {
    if (toKeep[n.id])
      n.color = n.originalColor;
    else
      n.color = '#eee';
  });

  SIGMA.graph.edges().forEach(function(e) {
    if (toKeep[e.source] && toKeep[e.target])
      e.color = e.originalColor;
    else
      e.color = '#eee';
  });

  SIGMA.refresh();
});

// undo the coloring to show neighbors when mouse out of a node
SIGMA.bind('outNode', function(e) {
  SIGMA.graph.nodes().forEach(function(n) {
    n.color = n.originalColor;
  });

  SIGMA.graph.edges().forEach(function(e) {
    e.color = e.originalColor;
  });

  SIGMA.refresh();
});



///////////////////////////////////
// Functions Related to Graphing //
///////////////////////////////////

// Called to push our changes from our internal data to SIGMA graph
function updateGraphNodes() {
  SIGMA.graph.nodes().forEach(function(n) {
    n.color = USERS[n.id].color;
    n.originalColor = USERS[n.id].originalColor;
    n.label = 'User ' +  n.id + " (v" + USERS[n.id].version + ')';
  });
  SIGMA.refresh();
  updateNodeVersionBreakdown();
}

function initRandomNodes() {
  toggleNodeInitButtonState(false);

  if (SIGMA.isForceAtlas2Running()) {
    SIGMA.killForceAtlas2();
  }

  SIGMA.graph.clear();

  var nodeCount = parseInt($("#nodeCount").val());
  var coachRatio = parseFloat($("#coachRatio").val());
  var coachMin = parseInt($("#coachMin").val());
  var coachMax = parseInt($("#coachMax").val());
  var studentCoachRatio = parseFloat($("#studentCoachRatio").val());
  var multipleCoachRatio = parseFloat($("#multipleCoachRatio").val());

  generateUsers(nodeCount, coachRatio, studentCoachRatio, multipleCoachRatio, coachMin, coachMax);

  // first add node
  var i = 0;

  // sorting the user neighbor count to make the forceatlas work a bit nicer
  sortedUsers = USERS.concat();
  sortedUsers.sort(function(a, b) {
    if (a.children.length > b.children.length) return -1;
    if (a.children.length < b.children.length) return 1;
    else return 0;
  });

  sortedUsers.forEach(function(user) {
    // don't add orphaned users
    if (user.children.length == 0 && !user.has_parent) {
      //return;
    }

    SIGMA.graph.addNode({
      id : user.id,
      label: user.label,
      x : i % 10,
      y : Math.floor(i/10),
      //x : chance.floating({min: 0, max: 1}),
      //y : chance.floating({min: 0, max: 1}),
      size: (user.children.length * 0.5) + 1,  // larger if has children
      color: user.color,
      originalColor: user.originalColor,
    });
    i++;
  });

  // then add edge
  sortedUsers.forEach(function(user) {
    user.children.forEach(function(child) {
      SIGMA.graph.addEdge({
        id: 'e_' + user.id + "_" + child,
        source: user.id,
        target: String(child)
      });
    });
  });

  SIGMA.refresh();
  updateNodeVersionBreakdown();

  if (!SIGMA.isForceAtlas2Running()) {
    SIGMA.startForceAtlas2({
      iterationsPerRender : 20,
    });
  }

  setTimeout(function() {
    SIGMA.killForceAtlas2();
    toggleNodeInitButtonState(true);
  }, 2000);
}

function removeOrphanedNodes() {
  toggleNodeInitButtonState(false);
  SIGMA.graph.nodes().forEach(function(n) {
    var user = USERS[n.id];
    if (user.children.length == 0 && !user.has_parent) {
      SIGMA.graph.dropNode(n.id);
    }
  });

  SIGMA.refresh();
  if (!SIGMA.isForceAtlas2Running()) {
    SIGMA.startForceAtlas2({
      iterationsPerRender : 20,
    });
  }

  setTimeout(function() {
    toggleNodeInitButtonState(true);
    SIGMA.killForceAtlas2();
  }, 2000);
}

// Used to generate the USERS data
function generateUsers(total, coach_rate, student_coach_rate, multi_coach_rate, coach_child_min, coach_child_max, default_version) {

  if (typeof(default_version) == "undefined") {
    default_version = "1";
  }

  userObjects = [];

  for(var i=0; i < total; i++) {
    var user = {
      id: String(i),
      label: 'User ' +  i + " (v" + default_version + ')',
      children: [],
      parents: [],
      version: default_version,
      color: VERSION_COLORS[default_version],
      originalColor: VERSION_COLORS[default_version],
      has_parent: false,
    };
    userObjects.push(user);
  };

  var possibleChildren = [];
  possibleChildren.writeIndices(total);
  var childrenWeight = [];
  childrenWeight.writeValues(total, 1);

  for(var i=0; i < total; i++) {
    // see if we randomly get a coach
    var coach_likelihood = coach_rate;

    if (userObjects[i].has_parent) {
      coach_likelihood = student_coach_rate;
    }

    if (Math.random() < coach_likelihood) {
      var coachChildCount = getRandomBetweenVals(coach_child_min, coach_child_max);
      var childrenSet = possibleChildren.concat();
      var weightSet = childrenWeight.concat();

      // update how likely to pick a node already w/ parent or children
      for(var j=0; j < userObjects.length; j++) {
        var hasChildren = userObjects[j].children.length > 0;
        var hasParent = userObjects[j].parents.length > 0;

        if (hasChildren) {
          if (hasParent) {
            weightSet[j] = student_coach_rate > multi_coach_rate ? multi_coach_rate : student_coach_rate;
          }
          else {
            weightSet[j] = student_coach_rate;
          }
        }
        else {
          if (hasParent) {
            weightSet[j] = multi_coach_rate;
          }
        }
      }

      // dont pick itself
      weightSet[i] = 0;

      var picked = 0;
      var ids = [];

      while(picked < coachChildCount) {
        var pickedId = chance.weighted(childrenSet, weightSet);

        // dont repeat pick
        var childrenSetIndex = childrenSet.indexOf(pickedId);
        weightSet[childrenSetIndex] = 0;

        // set child on parents
        ids.push(pickedId);

        // set parents on child
        if (!userObjects[pickedId]) {
          console.log(pickedId, userObjects[pickedId]);
        }
        if (userObjects[pickedId].parents.indexOf(i) == -1) {
          userObjects[pickedId].parents.push(i);
        }

        picked++;        
      }

      userObjects[i].children = ids;

      // used only for removing orphaned nodes quickly
      for(var c=0; c < ids.length; c++) {
        userObjects[ids[c]].has_parent = true;
      }
    }
  }

  USERS = userObjects;
}


// other shared func
function updateNodeVersionBreakdown() {
  var versions = {};

  if (USERS.length == 0) {
    $("#nodeVersionBreakdown").html('');
    $("#nodeVersionBreakdownContainer").addClass('hidden');
    return;
  }

  for(var i=0; i < USERS.length; i++) {
    var user = USERS[i];
    if (!versions.hasOwnProperty(user.version)) {
      versions[user.version] = 0;
    }
    versions[user.version] += 1;
  }

  var output = [];
  for(var i in versions) {
    output.push('Version ' + i + " : " + versions[i]);
  }

  $("#nodeVersionBreakdown").html(output.join('&nbsp;&nbsp;&nbsp;'));
  $("#nodeVersionBreakdownContainer").removeClass('hidden');
}

function toggleNodeInitButtonState(disabled) {
  $("#initRandomNodesBtn").prop('disabled', !disabled);
  $("#removeOrphanedNodesBtn").prop('disabled', !disabled);
}

function getRandomBetweenVals(min, max) {
  return chance.natural({min: min, max: max});
}

function print(text) {
  $("#output").val( text + "\n" + $("#output").val());
}